# apt-key deprecation

## Table of Contents
1. [Detecting the problem](#detecting-the-problem)
2. [Gathering information](#gathering-information-about-the-problem)
3. [Solution](#solution)
   1. [Removing the key](#removing-the-key)
   2. [Downloading and reinstalling the key](#reinstalling-the-removed-key)
   3. [Associating the key with the repository](#associating-the-repository-with-the-key)



## Detecting the problem

Running the command to update package list on Ubuntu 22.04 gives a warning about legacy key store.

command:
```bash 
sudo apt update
```

Example output:

```
Hit:1 http://hu.archive.ubuntu.com/ubuntu jammy InRelease
Hit:2 http://hu.archive.ubuntu.com/ubuntu jammy-updates InRelease
Hit:3 http://hu.archive.ubuntu.com/ubuntu jammy-backports InRelease
[...]
Hit:7 https://updates.signal.org/desktop/apt xenial InRelease
Hit:8 http://security.ubuntu.com/ubuntu jammy-security InRelease
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.
W: https://updates.signal.org/desktop/apt/dists/xenial/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
```

## Gathering information about the problem

Let's see the administator's manual page mentioned by the warning.

command:
```bash
man 8 apt-key
```
In the man page the DESCRIPTION section is giving details about the deprecation:


> DESCRIPTION
>
>      apt-key is used to manage the list of keys used by apt to authenticate packages. Packages which have been authenticated using these keys will be considered trusted.
>
>      Use of apt-key is deprecated, except for the use of apt-key del in maintainer scripts to remove existing keys from the main keyring. If such usage of apt-key is desired the additional installation of the GNU Privacy Guard suite (packaged in gnupg) is required.
> 
>       apt-key(8) will last be available in Debian 11 and Ubuntu 22.04.

The last line means in the next versions apt-key will be removed from the distribution.

Let's see the installed keys. List is shortened.

command:
```bash
apt-key list
```

Example output:

```
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
/etc/apt/trusted.gpg
--------------------
pub   rsa4096 2017-04-05 [SC]
      DBA3 6B51 81D0 C816 F630  E889 D980 A174 57F6 FB06
uid           [ unknown] Open Whisper Systems <support@whispersystems.org>
sub   rsa4096 2017-04-05 [E]
[...]
```

So, what do we know until now?

This key identified by the hexa string is not in a good place in that file. I know this is the one from the many keys, because it was checked after installation.


## Solution

The idea is to remove the key and add it again in a different place.

### Removing the key

To remove the key, the following can be used:

command
```bash
sudo apt-key del "DBA3 6B51 81D0 C816 F630  E889 D980 A174 57F6 FB06"
```

Example output:
```
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
OK
```
This needs verification, as sometimes a false 'OK' is returned while the key is not really removed. Use the `apt-key list` command to verify the key is really gone. I have tried without the double quotes and only with the first half of the hexa string based on some old articles. The result was 'OK', but the key remained in the list.

Let's see what happens, when the repository key is removed:

```bash
sudo apt update
```

Example output:
```
...
Hit:3 http://hu.archive.ubuntu.com/ubuntu jammy InRelease
Hit:4 http://hu.archive.ubuntu.com/ubuntu jammy-updates InRelease
Hit:5 http://hu.archive.ubuntu.com/ubuntu jammy-backports InRelease
Hit:6 https://updates.signal.org/desktop/apt xenial InRelease
Get:7 http://security.ubuntu.com/ubuntu jammy-security InRelease [110 kB]
[...]
Err:6 https://updates.signal.org/desktop/apt xenial InRelease
  The following signatures couldn't be verified because the public key is not available: NO_PUBKEY D980A17457F6FB06
Get:9 http://security.ubuntu.com/ubuntu jammy-security/main amd64 DEP-11 Metadata [41.7 kB]
Get:10 http://security.ubuntu.com/ubuntu jammy-security/universe amd64 DEP-11 Metadata [18.5 kB]
Fetched 171 kB in 1s (124 kB/s)             
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.
W: An error occurred during the signature verification. The repository is not updated and the previous index files will be used. GPG error: https://updates.signal.org/desktop/apt xenial InRelease: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY D980A17457F6FB06
W: Failed to fetch https://updates.signal.org/desktop/apt/dists/xenial/InRelease  The following signatures couldn't be verified because the public key is not available: NO_PUBKEY D980A17457F6FB06
W: Some index files failed to download. They have been ignored, or old ones used instead.
```
This means the previous step was successful, the key was removed. We no longer have the 'bad key' warning. It was taking a step back. The current error is that we don't have the key, so software cannot be downloaded from the repository. So it need to be installed again. Instructions are written on https://signal.org/download/#.

### Reinstalling the removed key

Visiting https://signal.org/download/#, clicking 'Download for Linux' the instructions pop up. Steps 1 and 2 are required. But as the configuration is already installed, a little modification is needed.

Command for downloading the key and saving to a file:
```bash
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
```
Example output:
```--2023-04-22 08:42:19--  https://updates.signal.org/desktop/apt/keys.asc
Resolving updates.signal.org (updates.signal.org)... 104.18.20.193, 104.18.21.193, 2606:4700::6812:15c1, ...
Connecting to updates.signal.org (updates.signal.org)|104.18.20.193|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 3090 (3.0K) [application/pgp-signature]
Saving to: ‘STDOUT’

-                                                    100%[=====================================================================================================================>]   3.02K  --.-KB/s    in 0.001s  

2023-04-22 08:42:20 (3.24 MB/s) - written to stdout [3090/3090]

```
From the saved file it is installed aka. redirected to it's final place:
```bash
cat signal-desktop-keyring.gpg | sudo tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
```
As the output is redirected to /dev/null, no visible output is presented.

At this point the key is installed, but not associated with the repository. The next step is to add configuration to the repository configuration to use the currently installed key. Here comes step 2:

### Associating the repository with the key

In the next section a 'best practice' is applied: verify before state, configure, verify after state.

Verify configuration file beforehand:
```bash 
cat /etc/apt/sources.list.d/signal-xenial.list
```
Example output:
```bash
deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main #Signal repository disabled on upgrade to jammy
```

At this point, the configuration need to be updated. Choose your favourite editor (nano, vim, sed, etc.) and add the "signed-by=<key-file.gpg>" to the apt configuration. If square brackets ("[]") is part is not in your configuration, add it between the "deb" and the repository URL. See the example output later here.


```bash
vim /etc/apt/sources.list.d/signal-xenial.list
```

After updating the configuration, verify it is as good as intended:

```bash
cat /etc/apt/sources.list.d/signal-xenial.list 
```

Example output:
```bash
deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main #Signal repository disabled on upgrade to jammy
```
The configuration seems good. The old configuration is removed, the key is downloaded and reinstalled/overwritten, and apt configuration is updated to use the freshly downloaded key. This is the time to test if it works as intended.

```bash
sudo apt update
```

Example output:
```
...                                    
Hit:3 https://updates.signal.org/desktop/apt xenial InRelease                                             
Hit:4 http://hu.archive.ubuntu.com/ubuntu jammy InRelease   
Hit:5 http://hu.archive.ubuntu.com/ubuntu jammy-updates InRelease
Hit:6 http://hu.archive.ubuntu.com/ubuntu jammy-backports InRelease
Hit:7 http://security.ubuntu.com/ubuntu jammy-security InRelease
...
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
All packages are up to date.
vata@blacklight:~$ 
```

The warning is gone, the configuration works.